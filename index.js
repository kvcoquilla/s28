// https://jsonplaceholder.typicode.com/todos

// 3
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json))

// 4
async function fetchData() {
	let result = await fetch('https://jsonplaceholder.typicode.com/todos');

	let json = await result.json(); //array of objects

	let arrayTitle = json.map(item => {
		let container = {};
		container[item.id] = item.title;
		return container
	})

	console.log(arrayTitle);
}

fetchData();

//5 & 6
fetch('https://jsonplaceholder.typicode.com/todos/80')
.then((response) => response.json())
.then((json) => console.log(json))

//7
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		userId: 11,
		id: 201,
		title: "New to do list Item",
		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

//8
fetch('https://jsonplaceholder.typicode.com/todos/15', {
	method: 'PUT',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		userId: 25,
		title: "Updated to do list item",
		completed: true
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

//9
fetch('https://jsonplaceholder.typicode.com/todos/30', {
	method: 'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: "Updated to do list item",
		description: "The quick brown fox jumps over a lazy dog.",
		status: "Not Started",
		date_completed: "10/05/2021",	
		userId: 25
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


//10
fetch('https://jsonplaceholder.typicode.com/todos/61', {
	method: 'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: "Patched to do list item",
		completed: true
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

//11
fetch('https://jsonplaceholder.typicode.com/todos/30', {
	method: 'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		status: "Complete",
		date_completed: "10/05/2021",	
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


//12 - POSTMAN

//14 - POSTMAN

//15 - POSTMAN
